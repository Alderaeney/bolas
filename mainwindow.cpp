#include "mainwindow.h"
#include "info.h"
#include "dinfobolas.h"
#include <QPainter>
#include <QTimer>
#include <math.h>
#include <QDebug>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QKeyEvent>
#include <QMessageBox>
#include <QMimeData>
#include <QDrag>
#include "dexamendam.h"
#include "grafico.h"
#include "pestanas.h"

MainWindow::MainWindow(QWidget * parent) : QMainWindow(parent) {
    QTimer * temporizador = new QTimer();

    this->setWindowTitle("Andreu Furió Benavent");

    temporizador->setInterval(15);
    temporizador->setSingleShot(false);
    temporizador->start();

    connect(temporizador, SIGNAL(timeout()), this, SLOT(slotRepintar()));

    resize(800, 600);

    gene.seed(time(NULL));

    for (int i = 0; i < 8; i++) {
        bolas.append(new Bola(gene.bounded(0, 800), gene.bounded(0, 600), gene.bounded(-4, 4), gene.bounded(-4, 4), 50, QColor(gene.bounded(0, 255), gene.bounded(0, 255), gene.bounded(0, 255))));
    }

    bolaJugador = bolas[0];
    bolaJugador->posX = bolaJugador->posY = 20;

    setMouseTracking(true);


    accionInfo = new QAction("Informacion");
    accionInfo->setShortcut(QKeySequence::HelpContents);
    connect(accionInfo, SIGNAL(triggered()), this, SLOT(slotInformacion()));
    menu = menuBar()->addMenu("menu");
    menu->addAction(accionInfo);

    accionInfoBolas = new QAction("Información bolas");
    connect(accionInfoBolas, SIGNAL(triggered()), this, SLOT(slotDInfoBolas()));
    menu->addAction(accionInfoBolas);

    accionExamen = new QAction("Examen DAM");
    connect(accionExamen, SIGNAL(triggered()), this, SLOT(slotDExamenDAM()));
    menu->addAction(accionExamen);

    accionControl = new QAction("Control");
    connect(accionControl, SIGNAL(triggered()), this, SLOT(slotControl()));
    menu->addAction(accionControl);

    accionGrafico = new QAction("Grafico");
    connect(accionGrafico, SIGNAL(triggered()), this, SLOT(slotGrafico()));
    menu->addAction(accionGrafico);

    examen = new DExamenDAM(&bolas, this);

    setAcceptDrops(true);

    posRatX = bolaJugador->posX;
    posRatY = bolaJugador->posY;
}

void MainWindow::paintEvent(QPaintEvent *event){
    QPainter pintor(this);

    for (int i = 0; i < bolas.size(); i++) {

        if (bolas[i]->resaltada){
            QBrush brush;
            brush.setStyle(Qt::FDiagPattern);
            pintor.setBrush(brush);
        } else {
            pintor.setBrush(QBrush(bolas[i]->color));
            pintor.setPen(bolas[i]->color);
        }
        if (bolas[i]->mostrarImagen){
            pintor.drawImage(bolas[i]->posX, bolas[i]->posY, bolas[i]->imagen);
        } else {
            pintor.drawEllipse(bolas[i]->posX, bolas[i]->posY, bolas[i]->diametro, bolas[i]->diametro);
        }
        int ancho = bolas[i]->diametro;
        float anchoVerde = (((float) bolas[i]->vida) / bolas[i]->vidaInicial) * (float) ancho;
        float anchoRojo = (ancho - (float) anchoVerde);
        pintor.setBrush(Qt::green);
        pintor.drawRect(bolas[i]->posX, bolas[i]->posY, anchoVerde, 3);
        pintor.setBrush(Qt::red);
        pintor.drawRect(bolas[i]->posX + anchoVerde, bolas[i]->posY, anchoRojo, 3);
    }
    if (bolaJugador != nullptr && bolaJugador->vida != 0){
        pintor.setBrush(Qt::black);
        pintor.drawEllipse(bolaJugador->posX + 20, bolaJugador->posY + 20, 10, 10);
    }

    for (int i = 0; i < pociones.size(); i++) {
        pintor.drawImage(pociones[i]->posX, pociones[i]->posY, pociones[i]->imagen);
    }

}

void MainWindow::slotRepintar(){
    for (int i = 0; i < bolas.size() - 1; i++) {
        for (int j = 0; j < bolas.size(); j++) {
            if (i != j) {
                if (bolas[i]->chocar(*bolas[j])){
                    bolas[i]->vida = (bolas[i]->vida - 5) < 0 ? 0 : bolas[i]->vida - 5;
                    bolas[i]->choques++;
                    bolas[j]->vida = (bolas[j]->vida - 5) < 0 ? 0 : bolas[j]->vida - 5;
                    bolas[i]->choques++;
                }
            }
        }
    }

    for (int i = 0; i < bolas.size(); i++) {
        bolas[i]->mover(width(), height());
        if (bolas[i]->vida == 0){
            if (bolaJugador != nullptr && bolas[i] == bolaJugador){
               bolaJugador = new Bola();
            }
            delete bolas[i];
            bolas.remove(i);
        }
    }
    this->update();

    if (gene.bounded(0, 1000) < 3) pociones.append(new Potion(gene.bounded(0, width()), gene.bounded(0, height()), 50, gene.bounded(0, 100)));

    for (int i = 0; i < pociones.size(); i++) {
        if (pociones[i]->chocar(*bolaJugador)) {
            delete pociones[i];
            pociones.remove(i);
        }
    }

    ratonMueveJugador();
}

void MainWindow::slotInformacion(){
    Informacion *dialogo = new Informacion(width(), height(), bolas.size(), this);
    dialogo->exec();
}

void MainWindow::keyPressEvent(QKeyEvent *e){
    if (bolaJugador != nullptr){
        int tecla = e->key();
        switch (tecla) {
        case Qt::Key_Left:
            bolaJugador->velX -= 0.3; break;
        case Qt::Key_Right:
            bolaJugador->velX += 0.3; break;
        case Qt::Key_Up:
            bolaJugador->velY -= 0.3; break;
        case Qt::Key_Down:
            bolaJugador->velY += 0.3; break;
        }
    }
}

void MainWindow::slotDInfoBolas(){
    DInfoBolas *dialogo = new DInfoBolas(bolas, this);
    dialogo->exec();
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *event){
    bolas.append(new Bola(event->x(), event->y(), gene.bounded(-2, 2), gene.bounded(-2, 2), 50));
}

void MainWindow::mouseMoveEvent(QMouseEvent *event){
    posRatX = event->x();
    posRatY = event->y();

    if (event->buttons() & Qt::LeftButton){
        int distance = (event->pos() - startPos).manhattanLength();
        if (distance >= QApplication::startDragDistance()){
            performDrag();
        }
    }
    QMainWindow::mouseMoveEvent(event);
}

void MainWindow::slotDExamenDAM(){
    examen->anadirTiempo(40);
    examen->show();
}

void MainWindow::slotControl(){
    WidgetPestanas *dialogo = new WidgetPestanas(&bolas, this);
    dialogo->show();
}

void MainWindow::dropEvent(QDropEvent *event){
    QMessageBox::warning(this, "DropEvent triggered", "Has arrastrado tu sucio puntero a esta ventana", QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel, QMessageBox::Save);
    bolas.append(new Bola(event->pos().x(), event->pos().y(), gene.bounded(-4, 4), gene.bounded(-4, 4), 50, event->mimeData()->urls().at(0).toLocalFile()));
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event){
    event->acceptProposedAction();
}

void MainWindow::mousePressEvent(QMouseEvent *event){
    if (event->button() == Qt::LeftButton){
        startPos = event->pos();
    }
    QMainWindow::mouseMoveEvent(event);
}

void MainWindow::performDrag(){
    QMimeData *data = new QMimeData;
    //data->setText("Prrro");
    QPixmap pixmap(size());
    this->render(&pixmap);
    data->setImageData(pixmap);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(data);
    drag->setPixmap(pixmap.scaled(50, 50));
    drag->exec(Qt::MoveAction);
}

void MainWindow::ratonMueveJugador(){
    if (bolaJugador != nullptr) {
        double incVelX = posRatX - bolaJugador->posX;
        incVelX = pow((incVelX * 0.0012), 3);
        double incVelY = posRatY - bolaJugador->posY;
        incVelY = pow((incVelY * 0.0012), 3);

        bolaJugador->velX += incVelX;
        bolaJugador->velY += incVelY;
    }
}

void MainWindow::slotGrafico(){
    Grafico *grafico = new Grafico(bolas);
    grafico->show();
}

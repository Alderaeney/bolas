#include "widgetbolas.h"

#include <QColorDialog>

WidgetBolas::WidgetBolas(Bola * bl, QWidget * parent) : QDialog (parent), bola(bl) {
    setupUi(this);

    connect(btnColor, SIGNAL(clicked()), this, SLOT(slotColor()));
    connect(btnParar, SIGNAL(clicked()), this, SLOT(slotParar()));
}

void WidgetBolas::slotColor(){
    QColorDialog *chooser = new QColorDialog();
    chooser->open();
    QColor color = chooser->getColor();
    bola->color = color;
}

void WidgetBolas::slotParar(){
    bola->velX = 0;
    bola->velY = 0;
}

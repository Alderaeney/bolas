#ifndef GRAFICO_H
#define GRAFICO_H

#include "bola.h"

#include <QDialog>
#include <QChartView>
#include <QBarSeries>
#include <QChart>
#include <QValueAxis>

QT_CHARTS_USE_NAMESPACE

class Grafico : public QDialog {
    Q_OBJECT
public:
    Grafico(QVector<Bola *> &bolas, QWidget * parent = 0);
    void cambiarAxis();
    void anadirASerie();


    QVector<Bola *> &bolas;
    QChart *chart;
    QChartView *chartView;
    QBarSeries *serie;
    QValueAxis *axisY, *axisX;

public slots:
    void slotTemporizador();

};


#endif // GRAFICO_H

/********************************************************************************
** Form generated from reading UI file 'widgetbolas.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETBOLAS_H
#define UI_WIDGETBOLAS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_WidgetBolas
{
public:
    QPushButton *btnParar;
    QPushButton *btnColor;

    void setupUi(QDialog *WidgetBolas)
    {
        if (WidgetBolas->objectName().isEmpty())
            WidgetBolas->setObjectName(QStringLiteral("WidgetBolas"));
        WidgetBolas->resize(157, 156);
        btnParar = new QPushButton(WidgetBolas);
        btnParar->setObjectName(QStringLiteral("btnParar"));
        btnParar->setGeometry(QRect(30, 30, 88, 27));
        btnColor = new QPushButton(WidgetBolas);
        btnColor->setObjectName(QStringLiteral("btnColor"));
        btnColor->setGeometry(QRect(30, 100, 88, 27));

        retranslateUi(WidgetBolas);

        QMetaObject::connectSlotsByName(WidgetBolas);
    } // setupUi

    void retranslateUi(QDialog *WidgetBolas)
    {
        WidgetBolas->setWindowTitle(QApplication::translate("WidgetBolas", "Dialog", nullptr));
        btnParar->setText(QApplication::translate("WidgetBolas", "Parar Bola", nullptr));
        btnColor->setText(QApplication::translate("WidgetBolas", "Elegir Color", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetBolas: public Ui_WidgetBolas {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETBOLAS_H

/********************************************************************************
** Form generated from reading UI file 'dinfobolas.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DINFOBOLAS_H
#define UI_DINFOBOLAS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_DInfoBolas
{
public:
    QDialogButtonBox *buttonBox;
    QListWidget *lista;
    QLabel *label;
    QComboBox *frequencia;

    void setupUi(QDialog *DInfoBolas)
    {
        if (DInfoBolas->objectName().isEmpty())
            DInfoBolas->setObjectName(QStringLiteral("DInfoBolas"));
        DInfoBolas->resize(609, 498);
        buttonBox = new QDialogButtonBox(DInfoBolas);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(240, 440, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        lista = new QListWidget(DInfoBolas);
        lista->setObjectName(QStringLiteral("lista"));
        lista->setGeometry(QRect(30, 20, 551, 281));
        label = new QLabel(DInfoBolas);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 360, 311, 19));
        frequencia = new QComboBox(DInfoBolas);
        frequencia->addItem(QString());
        frequencia->addItem(QString());
        frequencia->addItem(QString());
        frequencia->setObjectName(QStringLiteral("frequencia"));
        frequencia->setGeometry(QRect(400, 360, 181, 27));

        retranslateUi(DInfoBolas);
        QObject::connect(buttonBox, SIGNAL(accepted()), DInfoBolas, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DInfoBolas, SLOT(reject()));

        QMetaObject::connectSlotsByName(DInfoBolas);
    } // setupUi

    void retranslateUi(QDialog *DInfoBolas)
    {
        DInfoBolas->setWindowTitle(QApplication::translate("DInfoBolas", "Dialog", nullptr));
        label->setText(QApplication::translate("DInfoBolas", "Frequencia de Actualizacion", nullptr));
        frequencia->setItemText(0, QApplication::translate("DInfoBolas", "Alta", nullptr));
        frequencia->setItemText(1, QApplication::translate("DInfoBolas", "Media", nullptr));
        frequencia->setItemText(2, QApplication::translate("DInfoBolas", "Baja", nullptr));

    } // retranslateUi

};

namespace Ui {
    class DInfoBolas: public Ui_DInfoBolas {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DINFOBOLAS_H

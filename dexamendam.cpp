#include "dexamendam.h"

#include <QTimer>
#include <QColorDialog>
#include <QMessageBox>
#include <QDebug>

DExamenDAM::DExamenDAM(QVector<Bola *> *bls, QWidget * parent) : QDialog (parent), bolas(bls) {
    setupUi(this);

    QTimer * timer = new QTimer();
    timer->setInterval(1000);
    timer->setSingleShot(false);
    timer->start();
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTiempo()));

    numBolas = bolas->size();

    QTimer *timerBolas = new QTimer();
    timerBolas->setInterval(1000);
    timerBolas->setSingleShot(false);
    timerBolas->start();

    connect(timerBolas, SIGNAL(timeout()), this, SLOT(slotVectorCambiado()));

    rbYa->setChecked(true);

    spinBolas->setRange(0, bolas->size());
    connect(spinBolas, SIGNAL(valueChanged(int)), this, SLOT(slotCambiarLCD(int)));

    connect(botonRealizar, SIGNAL(clicked()), this, SLOT(slotRealizar()));
    connect(BotonFinalizar, SIGNAL(clicked()), this, SLOT(slotFinalizarAplicacion()));
    connect(rbYa, SIGNAL(clicked()), this, SLOT(slotCambiarRadio()));
    connect(rbRato, SIGNAL(clicked()), this, SLOT(slotCambiarRadio()));
}

void DExamenDAM::anadirTiempo(int tiempo){
    tiempoRestante += tiempo;
    if (tiempoRestante > 100) tiempoRestante = 100;
}

void DExamenDAM::slotTiempo(){
    progressBar->setValue(tiempoRestante);
    //progressBar->setFormat(QString::number(tiempoRestante) + " secs");
    tiempoRestante--;
    if (tiempoRestante == 0){
        this->close();
    }
}

void DExamenDAM::slotCambiarLCD(int num){
    lcdNumber->display(num);
}

void DExamenDAM::slotRealizar(){
    QString texto = comboAccion->itemText(comboAccion->currentIndex());
    if (texto == "Parar"){
        if (rbYa->isChecked()){
            slotPararBola();
        } else {
            QTimer *timer = new QTimer();
            timer->setInterval(2000);
            timer->setSingleShot(true);
            timer->start();
            connect(timer, SIGNAL(timeout()), this, SLOT(slotPararBola()));
        }
    } else if (texto == "Asignar color") {
        if (rbYa->isChecked()){
            slotCambiarColor();
        } else {
            QTimer *timer = new QTimer();
            timer->setInterval(2000);
            timer->setSingleShot(true);
            timer->start();
            connect(timer, SIGNAL(timeout()), this, SLOT(slotCambiarColor()));
        }
    }
}

void DExamenDAM::slotPararBola(){
    int num = spinBolas->value();
    bolas->at(num)->velX = 0;
    bolas->at(num)->velY = 0;
}

void DExamenDAM::slotCambiarColor(){
    QColorDialog *chooser = new QColorDialog();
    chooser->open();
    QColor color = chooser->getColor();
    bolas->at(spinBolas->value())->color = color;
}

void DExamenDAM::slotFinalizarAplicacion(){
    parentWidget()->close();
}

void DExamenDAM::slotCambiarRadio(){
    QRadioButton *boton = qobject_cast<QRadioButton*>(sender());
    if (boton->text() == "Ya"){
        int r = QMessageBox::warning(this, "Warning", "Vas a cambiar a Ya", QMessageBox::Ok | QMessageBox::Cancel);
        if (r == QMessageBox::Cancel){
            rbYa->setChecked(false);
            rbRato->setChecked(true);
        }
    } else {
        int r = QMessageBox::warning(this, "Warning", "Vas a cambiar a Rato", QMessageBox::Ok | QMessageBox::Cancel);
        if (r == QMessageBox::Cancel){
            rbYa->setChecked(true);
            rbRato->setChecked(false);
        }
    }
}

void DExamenDAM::slotVectorCambiado(){
    if (bolas->size() != numBolas){
        numBolas = bolas->size();
        spinBolas->setRange(0, bolas->size());
        //qDebug() << "Tamaño cambiado" + QString::number(bolas->size());
    }
    // qDebug() << "Entrando en el slot " + QString::number(bolas->size());
}

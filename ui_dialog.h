/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_informacion
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label;
    QLabel *label_2;
    QLabel *bolas;
    QLabel *tamano;

    void setupUi(QDialog *informacion)
    {
        if (informacion->objectName().isEmpty())
            informacion->setObjectName(QStringLiteral("informacion"));
        informacion->resize(400, 300);
        buttonBox = new QDialogButtonBox(informacion);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(informacion);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 60, 141, 19));
        label_2 = new QLabel(informacion);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 110, 141, 19));
        bolas = new QLabel(informacion);
        bolas->setObjectName(QStringLiteral("bolas"));
        bolas->setGeometry(QRect(190, 60, 141, 19));
        tamano = new QLabel(informacion);
        tamano->setObjectName(QStringLiteral("tamano"));
        tamano->setGeometry(QRect(190, 110, 161, 19));

        retranslateUi(informacion);
        QObject::connect(buttonBox, SIGNAL(accepted()), informacion, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), informacion, SLOT(reject()));

        QMetaObject::connectSlotsByName(informacion);
    } // setupUi

    void retranslateUi(QDialog *informacion)
    {
        informacion->setWindowTitle(QApplication::translate("informacion", "Dialog", nullptr));
        label->setText(QApplication::translate("informacion", "N\303\272mero de bolas", nullptr));
        label_2->setText(QApplication::translate("informacion", "Tama\303\261o Ventana", nullptr));
        bolas->setText(QApplication::translate("informacion", "bolas", nullptr));
        tamano->setText(QApplication::translate("informacion", "tamano", nullptr));
    } // retranslateUi

};

namespace Ui {
    class informacion: public Ui_informacion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "bola.h"
#include "dexamendam.h"
#include "potion.h"
#include <QRandomGenerator>

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget * parent = nullptr);
  void paintEvent(QPaintEvent *);
  void keyPressEvent(QKeyEvent *e);
  virtual void mouseDoubleClickEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent * event);
  virtual void dropEvent(QDropEvent *event);
  virtual void dragEnterEvent(QDragEnterEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void ratonMueveJugador();

private:
  double posX;
  double posY;
  int rotacio;
  double velX;
  double velY;
  int posRatY, posRatX;
  QPoint startPos;
  QVector<Bola *> bolas;
  QVector<Potion *> pociones;
  QRandomGenerator gene;
  QMenu *menu;
  QAction *accionInfo;
  QAction *accionInfoBolas;
  QAction *accionExamen;
  QAction *accionControl;
  QAction *accionGrafico;
  Bola *bolaJugador;
  DExamenDAM *examen;
  void performDrag();

public slots:
  void slotRepintar();
  void slotInformacion();
  void slotDInfoBolas();
  void slotDExamenDAM();
  void slotControl();
  void slotGrafico();
};

#endif

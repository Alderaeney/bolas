#include "potion.h"
#include "bola.h"
#include <math.h>

Potion::Potion(int pX, int pY, int diam, int v) : posX(pX), posY(pY), vida(v), diametro(diam) {
    imagen = QImage(":/images/potion.png").scaled(diametro, diametro);
}

double Potion::distancia(Bola *otra){
    double c1 = posX - otra->posX;
    double c2 = posY - otra->posY;

    return sqrt(pow(c1, 2) + pow(c2, 2));
}

bool Potion::chocar(Bola &otra){
    if (distancia(&otra) > diametro) return false;

    otra.anadirVida(vida);

    return true;
}

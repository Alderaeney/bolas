#ifndef DINFOBOLAS_H
#define DINFOBOLAS_H


#include "ui_dinfobolas.h"
#include <QDialog>
#include <QVector>
#include <bola.h>

class DInfoBolas : public QDialog, public Ui::DInfoBolas {
    Q_OBJECT
public:
    DInfoBolas(QVector<Bola*> &bls, QWidget * parent = 0);
    QVector<Bola*> &bolas;
    QTimer *temporizador;

public slots:
    void recargarInfo();
    void slotEscribirEnLaLista();
};

#endif // DINFOBOLAS_H

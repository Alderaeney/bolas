#ifndef POTION_H
#define POTION_H

#include "bola.h"
#include <QImage>

class Potion {
public:
    Potion(int pX, int pY, int diam, int v);
    int vida;
    QImage imagen;
    bool chocar(Bola &otra);
    double distancia(Bola *otra);
    int posX, posY, diametro;

};

#endif // POTION_H

#ifndef PESTANAS_H
#define PESTANAS_H

#include "ui_pestanas.h"
#include "bola.h"
#include <QDialog>
#include <QVector>

class WidgetPestanas : public QDialog, public Ui::WidgetPestanas {
    Q_OBJECT
public:
    WidgetPestanas(QVector<Bola *> * bls, QWidget * parent = 0);
    QVector<Bola *> * bolas;

public slots:
    void slotPararTodos();
    void slotResaltada();
};

#endif // PESTANAS_H

#ifndef DEXAMENDAM_H
#define DEXAMENDAM_H

#include "ui_DExamenDAM.h"
#include <QDialog>
#include <QVector>
#include <bola.h>

class DExamenDAM : public QDialog, public Ui::DExamenDAM {
    Q_OBJECT
public:
    DExamenDAM(QVector<Bola*> *bls, QWidget * parent = 0);
    QVector<Bola *> *bolas;
    int tiempoRestante;
    int numBolas;

    void anadirTiempo(int tiempo);

public slots:
    void slotTiempo();
    void slotCambiarLCD(int);
    void slotRealizar();
    void slotPararBola();
    void slotFinalizarAplicacion();
    void slotCambiarColor();
    void slotCambiarRadio();
    void slotVectorCambiado();
};

#endif // DEXAMENDAM_H

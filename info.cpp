#include "info.h"

Informacion::Informacion(int ancho, int alto, int numBolas, QWidget * parent) : QDialog (parent) {
    setupUi(this);
    bolas->setText(QString::number(numBolas));
    tamano->setText(QString::number(ancho) + "x" + QString::number(alto));
}

#ifndef WIDGETBOLAS_H
#define WIDGETBOLAS_H

#include "ui_widgetbolas.h"
#include <QDialog>
#include "bola.h"

class WidgetBolas : public QDialog, public Ui::WidgetBolas {
    Q_OBJECT
public:
    WidgetBolas(Bola * bl, QWidget * parent = 0);
    Bola * bola;

public slots:
    void slotParar();
    void slotColor();
};

#endif // WIDGETBOLAS_H

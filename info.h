#ifndef INFO_H
#define INFO_H

#include "ui_dialog.h"
#include <QDialog>

class Informacion : public QDialog, public Ui::informacion {
    Q_OBJECT
public:
    Informacion(int ancho, int alto, int numBolas, QWidget * parent = 0);
};

#endif // INFO_H

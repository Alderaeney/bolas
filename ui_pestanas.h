/********************************************************************************
** Form generated from reading UI file 'pestanas.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PESTANAS_H
#define UI_PESTANAS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetPestanas
{
public:
    QDialogButtonBox *buttonBox;
    QTabWidget *pestanas;
    QWidget *tab;
    QWidget *tab_2;
    QPushButton *btnPararTodas;

    void setupUi(QDialog *WidgetPestanas)
    {
        if (WidgetPestanas->objectName().isEmpty())
            WidgetPestanas->setObjectName(QStringLiteral("WidgetPestanas"));
        WidgetPestanas->resize(400, 300);
        buttonBox = new QDialogButtonBox(WidgetPestanas);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(40, 260, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        pestanas = new QTabWidget(WidgetPestanas);
        pestanas->setObjectName(QStringLiteral("pestanas"));
        pestanas->setGeometry(QRect(20, 20, 361, 201));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        pestanas->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        pestanas->addTab(tab_2, QString());
        btnPararTodas = new QPushButton(WidgetPestanas);
        btnPararTodas->setObjectName(QStringLiteral("btnPararTodas"));
        btnPararTodas->setGeometry(QRect(290, 230, 88, 27));

        retranslateUi(WidgetPestanas);
        QObject::connect(buttonBox, SIGNAL(accepted()), WidgetPestanas, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), WidgetPestanas, SLOT(reject()));

        QMetaObject::connectSlotsByName(WidgetPestanas);
    } // setupUi

    void retranslateUi(QDialog *WidgetPestanas)
    {
        WidgetPestanas->setWindowTitle(QApplication::translate("WidgetPestanas", "Dialog", nullptr));
        pestanas->setTabText(pestanas->indexOf(tab), QApplication::translate("WidgetPestanas", "Tab 1", nullptr));
        pestanas->setTabText(pestanas->indexOf(tab_2), QApplication::translate("WidgetPestanas", "Tab 2", nullptr));
        btnPararTodas->setText(QApplication::translate("WidgetPestanas", "Parar todas", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetPestanas: public Ui_WidgetPestanas {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PESTANAS_H

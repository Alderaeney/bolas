#include "dinfobolas.h"

#include <QTimer>

DInfoBolas::DInfoBolas(QVector<Bola *> &bls, QWidget * parent) : QDialog (parent), bolas(bls) {
    setupUi(this);
    slotEscribirEnLaLista();

    connect(frequencia, SIGNAL(currentIndexChanged(int)), this, SLOT(recargarInfo()));
    temporizador = new QTimer();
    temporizador->setInterval(500);
    temporizador->setSingleShot(false);
    temporizador->start();
    connect(temporizador, SIGNAL(timeout()), this, SLOT(slotEscribirEnLaLista()));
}

void DInfoBolas::recargarInfo(){
    QString posicion = frequencia->itemText(frequencia->currentIndex());
    if (posicion == "Alta"){
        temporizador->setInterval(500);
    } else if (posicion == "Media") {
        temporizador->setInterval(1000);
    } else {
        temporizador->setInterval(1500);
    }
}

void DInfoBolas::slotEscribirEnLaLista(){
    lista->clear();
    for (int i = 0; i < bolas.size(); i++) {
        QListWidgetItem *item = new QListWidgetItem("Pos x: " + QString::number(bolas[i]->posX) + " Pos y: " + QString::number(bolas[i]->posY) + " Vel x: " + QString::number(bolas[i]->velX) + " Vel y: " + QString::number(bolas[i]->velY));
        item->setBackgroundColor(bolas[i]->color);
        lista->addItem(item);
    }
}

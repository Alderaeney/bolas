#include "grafico.h"

#include <QTimer>
#include <QBarSet>
#include <QHBoxLayout>

Grafico::Grafico(QVector<Bola *> &bolas, QWidget *parent): QDialog (parent), bolas(bolas) {
    QTimer *temporizador = new QTimer;
    temporizador->setSingleShot(false);
    temporizador->setInterval(500);

    connect(temporizador, SIGNAL(timeout()), this, SLOT(slotTemporizador()));
    temporizador->start();


    chart = new QChart;
    chartView = new QChartView(chart);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(chartView);

    setLayout(layout);

    serie = new QBarSeries();

    axisY = new QValueAxis;
    axisY->setRange(0, 5);
    axisY->setTitleText("Choques");
    chart->addAxis(axisY, Qt::AlignLeft);


    anadirASerie();
    chart->addSeries(serie);
    serie->attachAxis(axisY);
    resize(800, 600);
}

void Grafico::slotTemporizador(){
    foreach (QBarSet *set, serie->barSets()){
        serie->remove(set);
    }
    anadirASerie();
    cambiarAxis();
}

void Grafico::anadirASerie(){
    for (int i = 0; i < bolas.size(); i++) {
        QBarSet *set = new QBarSet("Bola " + QString::number(i));
        set->append(bolas[i]->choques);
        set->setColor(bolas[i]->color);
        serie->append(set);
    }
}

void Grafico::cambiarAxis(){
    int max = 0;
    foreach(Bola *bola, bolas){
        if (bola->choques > max) max = bola->choques;
    }
    axisY->setMax(max + 2);
}

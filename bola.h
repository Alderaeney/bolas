#ifndef BOLA_H
#define BOLA_H

#include <QColor>
#include <QPainter>


class Bola {
public:
    double posX, posY, velX, velY;
    int vidaInicial = 100, vida = 100, diametro, choques = 0;
    QColor color;
    bool resaltada = false;
    bool mostrarImagen = false;
    QImage imagen;
    Bola(double pX, double pY, double vX, double vY, int diam, QColor col);
    Bola(double pX, double pY, double vX, double vY, int diam);
    Bola(double pX, double pY, double vX, double vY, int diam, QString ruta);
    Bola();

    void mover(int ancho, int alto);
    double distancia(Bola *otra);
    bool chocar(Bola &otra);
    void pintarVida(QPainter pintor);
    void anadirVida(int vida);
};

#endif // BOLA_H

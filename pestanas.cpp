#include "pestanas.h"

#include <QVector>
#include "widgetbolas.h"
#include <QDebug>


WidgetPestanas::WidgetPestanas(QVector<Bola *> * bls, QWidget * parent) : QDialog (parent), bolas(bls) {
    setupUi(this);

    pestanas->clear();

    for (int i = 0; i < bolas->size(); i++) {
        pestanas->addTab(new WidgetBolas(bolas->at(i), this), "Bola " + QString::number(i));
    }

    connect(btnPararTodas, SIGNAL(clicked()), this, SLOT(slotPararTodos()));
    connect(pestanas, SIGNAL(currentChanged(int)), this, SLOT(slotResaltada()));
}

void WidgetPestanas::slotPararTodos(){
    for (int i = 0; i < pestanas->count(); i++) {
        WidgetBolas *widget = qobject_cast<WidgetBolas *>(pestanas->widget(i));
        widget->slotParar();
    }
}

void WidgetPestanas::slotResaltada(){
    for (int i = 0; i< pestanas->count();i++) {
        WidgetBolas *widget = qobject_cast<WidgetBolas *> (pestanas->widget(i));
        if (widget != NULL ) widget->bola->resaltada = false;
            else qDebug() << "fallo en pestaña" << i << endl;
    }

    WidgetBolas *widget = qobject_cast<WidgetBolas *>(pestanas->currentWidget());
    widget->bola->resaltada = true;
}

#include "bola.h"
#include <math.h>

Bola::Bola(double pX, double pY, double vX, double vY, int diam, QColor col) :
    Bola(pX, pY, vX, vY, diam) {
    color = col;
}

Bola::Bola(double pX, double pY, double vX, double vY, int diam) :
    posX(pX), posY(pY), velX(vX), velY(vY), diametro(diam) {
    srand(time(NULL));
    color = QColor(rand() % 256, rand() % 256, rand() % 256);
}

Bola::Bola(double pX, double pY, double vX, double vY, int diam, QString ruta): Bola(pX, pY, vX, vY, diam) {
    imagen = QImage(ruta).scaled(diametro, diametro);
    mostrarImagen = true;
}
Bola::Bola(){}

void Bola::mover(int ancho, int alto){
    if (posX > ancho - 50){
        velX = -fabs(velX);
    } else if (posX < 0){
        velX = fabs(velX);
    }

    if (posY > alto - 50) {
        velY = -fabs(velY);
    } else if (posY < 0){
        velY = +fabs(velY);
    }

    posX += velX;
    posY += velY;
}

double Bola::distancia(Bola *otra){
    double c1 = posX - otra->posX;
    double c2 = posY - otra->posY;

    return sqrt(pow(c1, 2) + pow(c2, 2));
}

bool Bola::chocar(Bola &otra){
    if (distancia(&otra) > diametro) return false;

    Bola *izquierda;
    Bola *derecha;

    if (posX > otra.posX){
        derecha = this;
        izquierda = &otra;
    } else {
        derecha = &otra;
        izquierda = this;
    }

    if (izquierda->velX > derecha->velX){
        double aux = izquierda->velX;
        izquierda->velX = derecha->velX;
        derecha->velX = aux;
    }

    Bola *arriba;
    Bola *abajo;

    if (posY > otra.posY){
        abajo = this;
        arriba = &otra;
    } else {
        abajo = &otra;
        arriba = this;
    }

    if (arriba->velY > abajo->velY){
        double aux = arriba->velY;
        arriba->velY = abajo->velY;
        abajo->velY = aux;
    }

    return true;
}

void Bola::anadirVida(int v){
    vida += v;
    if (vida > 100){
        vida = 100;
    }
}
